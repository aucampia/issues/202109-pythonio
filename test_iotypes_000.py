import io
import typing


def accept_text_io(obj: typing.TextIO) -> None:
    assert isinstance(obj, io.TextIOBase)


def accept_binary_io(obj: typing.BinaryIO) -> None:
    assert isinstance(obj, io.RawIOBase) or isinstance(obj, io.BufferedIOBase)
