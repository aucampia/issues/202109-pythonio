import io
import pathlib
import random
import tempfile
import typing
import unittest


class DummyBaseIO(typing.IO[typing.Any], io.IOBase):
    pass


class DummyTextIO(typing.TextIO, io.TextIOBase):
    pass


class DummyRawIO(typing.BinaryIO, io.RawIOBase):
    pass


class DummyRawIO(typing.BinaryIO, io.BufferedIOBase):
    pass


def terminate_anystr_io(obj: typing.IO[typing.AnyStr]) -> None:
    pass


def terminate_text_io(obj: typing.TextIO) -> None:
    pass


def terminate_binary_io(obj: typing.BinaryIO) -> None:
    pass


def terminate_iobase(obj: io.IOBase) -> None:
    pass


def terminate_raw_iobase(obj: io.RawIOBase) -> None:
    pass


def terminate_buffered_iobase(obj: io.BufferedIOBase) -> None:
    pass


def terminate_text_iobase(obj: io.TextIOBase) -> None:
    pass


def accept_anystr_io(obj: typing.IO[typing.AnyStr]) -> None:
    assert isinstance(obj, io.IOBase)


def terminate_text_io(obj: typing.TextIO) -> None:
    assert isinstance(obj, io.TextIOBase)


def terminate_binary_io(obj: typing.BinaryIO) -> None:
    assert isinstance(obj, io.RawIOBase) or isinstance(obj, io.BufferedIOBase)


class TestOpen(unittest.TestCase):
    def setUp(self) -> None:
        self._tmp_path = tempfile.TemporaryDirectory()
        self.tmp_path = pathlib.Path(self._tmp_path.name)
        self.tmp_file = self.tmp_path / "file"

    def tearDown(self) -> None:
        self._tmp_path.cleanup()

    def test_something(self) -> None:
        def foo(value: typing.IO[typing.AnyStr]) -> None:
            assert isinstance(value, io.IOBase)

    def test_open_any_stream(self) -> None:
        mode = random.choice(["w", "wb"])
        with self.tmp_file.open(mode) as text_stream:
            any_io: typing.IO[typing.Any] = text_stream  # noqa: F841
            io_base: io.IOBase = text_stream  # noqa: F841
            assert isinstance(text_stream, io.IOBase)

    def test_open_text_stream(self) -> None:
        with self.tmp_file.open("w") as text_stream:
            text_io: typing.TextIO = text_stream  # noqa: F841
            text_io_base: io.TextIOBase = text_stream  # noqa: F841
            assert isinstance(text_stream, io.TextIOBase)

    def test_open_buffered_stream(self) -> None:
        with self.tmp_file.open("wb") as buffered_stream:
            binary_io: typing.BinaryIO = buffered_stream  # noqa: F841
            buffered_io_base: io.BufferedIOBase = buffered_stream  # noqa: F841
            assert isinstance(buffered_stream, io.BufferedIOBase)

    def test_open_raw_stream(self) -> None:
        with self.tmp_file.open("wb", buffering=0) as raw_stream:
            binary_io: typing.BinaryIO = raw_stream  # noqa: F841
            raw_io_base: io.RawIOBase = raw_stream  # noqa: F841
            assert isinstance(binary_io, io.RawIOBase)


if __name__ == "__main__":
    unittest.main()
