import io
import pathlib
import typing as t
from typing import overload


@overload
def as_textio(anyio: io.TextIOBase, encoding: None = ...) -> io.TextIOBase:
    ...


@overload
def as_textio(
    anyio: t.Union[io.RawIOBase, io.BufferedIOBase], encoding: t.Optional[str] = ...
) -> io.TextIOBase:
    ...


def as_textio(
    anyio: t.Union[io.RawIOBase, io.BufferedIOBase, io.TextIOBase],
    encoding: t.Optional[str] = None,
) -> io.TextIOBase:
    if hasattr(anyio, "encoding"):
        return t.cast(io.TextIOBase, anyio)
    return io.TextIOWrapper(
        t.cast(t.Union[io.RawIOBase, io.BufferedIOBase], anyio), encoding=encoding
    )


def test_as_textio_text(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("w") as text_stream:
        text_io: t.TextIO = text_stream
        assert text_io is text_stream
        assert as_textio(text_stream) is text_stream
        as_textio(text_stream).write("Test")
        text_stream.flush()
        assert tmp_file.read_text() == "Test"
        assert isinstance(text_io, io.TextIOBase)


def test_as_textio_buffered_stream(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("wb") as buffered_stream:
        binary_io: t.BinaryIO = buffered_stream
        assert binary_io is buffered_stream
        as_textio(buffered_stream).write("Test")
        assert tmp_file.read_text() == "Test"
        assert isinstance(buffered_stream, io.BufferedIOBase)


def test_as_textio_raw_stream(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("wb", buffering=0) as raw_stream:
        binary_io: t.BinaryIO = raw_stream
        assert binary_io is raw_stream
        as_textio(raw_stream).write("Test")
        assert tmp_file.read_text() == "Test"
        assert isinstance(binary_io, io.RawIOBase)
