import io
import pathlib
import typing as t


def test_open_text_stream(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("w") as text_stream:
        text_io: t.TextIO = text_stream  # noqa: F841
        text_io_base: io.TextIOBase = text_stream  # noqa: F841
        assert isinstance(text_stream, io.TextIOBase)


def test_open_buffered_stream(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("wb") as buffered_stream:
        binary_io: t.BinaryIO = buffered_stream  # noqa: F841
        buffered_io_base: io.BufferedIOBase = buffered_stream  # noqa: F841
        assert isinstance(buffered_stream, io.BufferedIOBase)


def test_open_raw_stream(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("wb", buffering=0) as raw_stream:
        binary_io: t.BinaryIO = raw_stream  # noqa: F841
        raw_io_base: io.RawIOBase = raw_stream  # noqa: F841
        assert isinstance(binary_io, io.RawIOBase)
