import io
import typing


class DummyBaseIO0(typing.IO[typing.AnyStr], io.IOBase):
    pass


class DummyBaseIO1(typing.IO[typing.Any], io.IOBase):
    pass


class DummyTextIO(typing.TextIO, io.TextIOBase):
    pass


class DummyRawIO(typing.BinaryIO, io.RawIOBase):
    pass


class DummyRawIO(typing.BinaryIO, io.BufferedIOBase):
    pass
