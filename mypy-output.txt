poetry run mypy --show-error-codes --show-error-context \
	test_as_textio_v1.py test_io.py test_other.py
test_as_textio_v1.py: note: In function "as_textio":
test_as_textio_v1.py:26: error: Argument 1 to "TextIOWrapper" has incompatible type "Union[RawIOBase, BufferedIOBase]"; expected "IO[bytes]"  [arg-type]
test_as_textio_v1.py: note: In function "test_as_textio_text":
test_as_textio_v1.py:35: error: Subclass of "TextIO" and "TextIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_as_textio_v1.py:36: error: Statement is unreachable  [unreachable]
test_as_textio_v1.py:37: error: Statement is unreachable  [unreachable]
test_as_textio_v1.py: note: In function "test_as_textio_buffered_stream":
test_as_textio_v1.py:45: error: Subclass of "BinaryIO" and "BufferedIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_as_textio_v1.py:46: error: Statement is unreachable  [unreachable]
test_as_textio_v1.py:47: error: Statement is unreachable  [unreachable]
test_as_textio_v1.py: note: In function "test_as_textio_raw_stream":
test_as_textio_v1.py:55: error: Subclass of "BinaryIO" and "RawIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_as_textio_v1.py:56: error: Statement is unreachable  [unreachable]
test_as_textio_v1.py:57: error: Statement is unreachable  [unreachable]
test_other.py: note: In function "test_open_binary_buffered":
test_other.py:23: error: Incompatible types in assignment (expression has type "BinaryIO", variable has type "BufferedIOBase")  [assignment]
test_other.py:32: error: Non-overlapping identity check (left operand type: "BinaryIO", right operand type: "BufferedIOBase")  [comparison-overlap]
test_other.py:35: error: Subclass of "BinaryIO" and "RawIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_other.py:38: error: Subclass of "BinaryIO" and "BufferedIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_other.py: note: In function "test_open_binary_unbuffered":
test_other.py:46: error: Incompatible types in assignment (expression has type "BinaryIO", variable has type "RawIOBase")  [assignment]
test_other.py:55: error: Non-overlapping identity check (left operand type: "BinaryIO", right operand type: "RawIOBase")  [comparison-overlap]
test_other.py:58: error: Subclass of "BinaryIO" and "RawIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_other.py:61: error: Statement is unreachable  [unreachable]
test_other.py: note: In function "test_open_text":
test_other.py:68: error: Incompatible types in assignment (expression has type "TextIO", variable has type "TextIOBase")  [assignment]
test_other.py:77: error: Non-overlapping identity check (left operand type: "TextIO", right operand type: "TextIOBase")  [comparison-overlap]
test_other.py:80: error: Subclass of "TextIO" and "TextIOBase" cannot exist: would have incompatible method signatures  [unreachable]
Found 21 errors in 2 files (checked 3 source files)
