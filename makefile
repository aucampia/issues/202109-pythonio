py_source=$(wildcard *.py)

mypy:
	poetry run mypy --show-error-codes --show-error-context \
		$(py_source)


test:
	poetry run pytest $(py_source)

validate-static-other:
	poetry run isort --check $(py_source)
	poetry run black --check $(py_source)
	poetry run flake8 $(py_source)


validate-static: validate-static-other mypy
validate: test validate-static

validate-fix:
	poetry run isort $(py_source)
	poetry run black $(py_source)
