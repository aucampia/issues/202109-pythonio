# mypy thinks values returned by open are not subclasses/instances of io.RawIOBase, io.BufferedIOBase or io.TextIOBase

I am trying to make the rdflib [ResultSerializer.serialize][rdflib/ResultSerializer.serialize] and [Graph.serialize][rdflib/Graph.serialize] methods more consistent. And in doing so I would like to make them accept and write to objects that are both:
 - BinaryIO "like" streams, specifically objects like those returned from `open(...,"wb", buffering=0)` and `open(...,"wb")` [[open]].
 - TextIO "like" streams, specifically objects like those returned from `open(...,"w")` [[open]].

As rdflib supports python 3.7 or later, most of the statements are valid for python 3.7, unless stated otherwise. The code given here can be found [here][202109-pythonio]

My initial approach was to just something like this:

```python
@overload
def as_textio(anyio: io.TextIOBase, encoding: None = ...) -> io.TextIOBase:
    ...


@overload
def as_textio(
    anyio: t.Union[io.RawIOBase, io.BufferedIOBase], encoding: t.Optional[str] = ...
) -> io.TextIOBase:
    ...


def as_textio(
    anyio: t.Union[io.RawIOBase, io.BufferedIOBase, io.TextIOBase],
    encoding: t.Optional[str] = None,
) -> io.TextIOBase:
    if hasattr(anyio, "encoding"):
        return t.cast(io.TextIOBase, anyio)
    return io.TextIOWrapper(
        t.cast(t.Union[io.RawIOBase, io.BufferedIOBase], anyio), encoding=encoding
    )
```

My reasoning for this was based on python's documentation of the [open] function:

> The type of file object returned by the open() function depends on the mode. When open() is used to open a file in a text mode ('w', 'r', 'wt', 'rt', etc.), it returns a subclass of [io.TextIOBase][io.TextIOBase] (specifically io.TextIOWrapper). When used to open a file in a binary mode with buffering, the returned class is a subclass of [io.BufferedIOBase][io.BufferedIOBase]. The exact class varies: in read binary mode, it returns an io.BufferedReader; in write binary and append binary modes, it returns an io.BufferedWriter, and in read/write mode, it returns an io.BufferedRandom. When buffering is disabled, the raw stream, a subclass of [io.RawIOBase][io.RawIOBase], io.FileIO, is returned.

This passes the following tests:

```python
def test_as_textio_text(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("w") as text_stream:
        text_io: t.TextIO = text_stream
        assert text_io is text_stream
        as_textio(text_stream).write("Test")
        assert tmp_file.read_text() == "Test"
        assert isinstance(text_io, io.TextIOBase)


def test_as_textio_buffered_stream(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("wb") as buffered_stream:
        binary_io: t.BinaryIO = buffered_stream
        assert binary_io is buffered_stream
        as_textio(buffered_stream).write("Test")
        assert tmp_file.read_text() == "Test"
        assert isinstance(buffered_stream, io.BufferedIOBase)


def test_as_textio_raw_stream(tmp_path: pathlib.Path) -> None:
    tmp_file = tmp_path / "file"
    with tmp_file.open("wb", buffering=0) as raw_stream:
        binary_io: t.BinaryIO = raw_stream
        assert binary_io is raw_stream
        assert tmp_file.read_text() == "Test"
        as_textio(raw_stream).write("Test")
        assert isinstance(binary_io, io.RawIOBase)
```

However, mypy is unhappy about this code (full log [here][202109-pythonio/mypy-output.txt]):
- mypy thinks `text_stream`, is of [`typing.TextIO`][typing.TextIO] type, but not of [`io.TextIOBase`][io.TextIOBase] type:
  - `as_textio(text_stream).write("Test")` -> error: No overload variant of "as_textio" matches argument type "TextIO"  [call-overload]
  - `assert isinstance(text_io, io.TextIOBase)` -> error: Subclass of "TextIO" and "TextIOBase" cannot exist: would have incompatible method signatures  [unreachable]
- mypy thinks `buffered_stream`, is of [`typing.BinaryIO`][typing.BinaryIO] type, but not [`io.BufferedIOBase`][io.BufferedIOBase] type:
  - `as_textio(buffered_stream).write("Test")` -> error: No overload variant of "as_textio" matches argument type "BinaryIO"  [call-overload]
  - `assert isinstance(buffered_stream, io.BufferedIOBase)` -> error: Subclass of "BinaryIO" and "BufferedIOBase" cannot exist: would have incompatible method signatures  [unreachable]
- mypy thinks `raw_stream`, is of [`typing.BinaryIO`][typing.BinaryIO] type, but not of [`io.RawIOBase`][io.BufferedIOBase] type:
  - `as_textio(raw_stream).write("Test")` -> error: No overload variant of "as_textio" matches argument type "BinaryIO"  [call-overload]
  - `assert isinstance(binary_io, io.RawIOBase)` -> error: Subclass of "BinaryIO" and "RawIOBase" cannot exist: would have incompatible method signatures  [unreachable]

I think this is because of the following things:
-

See also:
 - https://github.com/python/typeshed/issues/6061


  [rdflib/ResultSerializer.serialize]: https://github.com/RDFLib/rdflib/blob/master/rdflib/query.py#L345
  [rdflib/Graph.serialize]: https://github.com/RDFLib/rdflib/blob/master/rdflib/query.py#L209

  [typeshed/stdlib/io.pyi]: https://github.com/python/mypy/blob/v0.910/mypy/typeshed/stdlib/io.pyi
  [typeshed/stdlib/builtins.pyi]: https://github.com/python/mypy/blob/v0.910/mypy/typeshed/stdlib/builtins.pyi


  [io.RawIOBase]: https://docs.python.org/3.7/library/io.html#io.RawIOBase
  [io.BufferedIOBase]: https://docs.python.org/3.7/library/io.html#io.BufferedIOBase
  [io.TextIOBase]: https://docs.python.org/3.7/library/io.html#io.TextIOBase
  [io.FileIO]: https://docs.python.org/3.7/library/io.html#io.FileIO

  [typing.TextIO]: https://docs.python.org/3.7/library/typing.html#typing.TextIO
  [typing.BinaryIO]: https://docs.python.org/3.7/library/typing.html#typing.BinaryIO

  [open]: https://docs.python.org/3.7/library/functions.html#open

  [202109-pythonio]: https://gitlab.com/aucampia/issues/202109-pythonio/-/tree/master/
  [202109-pythonio/mypy-output.txt]: https://gitlab.com/aucampia/issues/202109-pythonio/-/blob/master/mypy-output.txt
