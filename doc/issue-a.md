# mypy thinks objects returned by open are not instances of io.{Raw,Buffered,Text}IOBase

It seems like mypy is not honoring the [typeshed Path.open overrides](https://github.com/python/mypy/blob/v0.910/mypy/typeshed/stdlib/pathlib.pyi#L83-L139) exactly, as from these I would expect mypy to think the values returned by open was:
- `io.TextIOWrapper` for `.open("w")`
- `io.BufferedWriter` for `.open("wb")`
- `io.FileIO` for `.open("wb", buffering=0)`
But instead, mypy thinks the values returned are:
- `typing.TextIO` for `.open("w")`
- `typing.BinaryIO` for `.open("wb")`
- `typing.BinaryIO` for `.open("wb", buffering=0)`

This can be seen when running mypy on the following code:

```python
import io
import pathlib
import tempfile
import typing
import unittest


class TestOpen(unittest.TestCase):
    def setUp(self) -> None:
        self._tmp_path = tempfile.TemporaryDirectory()
        self.tmp_path = pathlib.Path(self._tmp_path.name)
        self.tmp_file = self.tmp_path / "file"

    def tearDown(self) -> None:
        self._tmp_path.cleanup()

    def test_open_text_stream(self) -> None:
        with self.tmp_file.open("w") as text_stream:
            text_io: typing.TextIO = text_stream  # noqa: F841
            text_io_base: io.TextIOBase = text_stream  # noqa: F841
            assert isinstance(text_stream, io.TextIOBase)

    def test_open_buffered_stream(self) -> None:
        with self.tmp_file.open("wb") as buffered_stream:
            binary_io: typing.BinaryIO = buffered_stream  # noqa: F841
            buffered_io_base: io.BufferedIOBase = buffered_stream  # noqa: F841
            assert isinstance(buffered_stream, io.BufferedIOBase)

    def test_open_raw_stream(self) -> None:
        with self.tmp_file.open("wb", buffering=0) as raw_stream:
            binary_io: typing.BinaryIO = raw_stream  # noqa: F841
            raw_io_base: io.RawIOBase = raw_stream  # noqa: F841
            assert isinstance(binary_io, io.RawIOBase)


if __name__ == "__main__":
    unittest.main()
```

All the tests in the code pass succesfully, however, if I run mypy on the source I get the following errors:

```
$ poetry run mypy --show-error-codes --show-error-context  test_open_ut.py
test_open_ut.py: note: In member "test_open_text_stream" of class "TestOpen":
test_open_ut.py:20: error: Incompatible types in assignment (expression has type "TextIO", variable has type "TextIOBase")  [assignment]
test_open_ut.py:21: error: Subclass of "TextIO" and "TextIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_open_ut.py: note: In member "test_open_buffered_stream" of class "TestOpen":
test_open_ut.py:26: error: Incompatible types in assignment (expression has type "BinaryIO", variable has type "BufferedIOBase")  [assignment]
test_open_ut.py:27: error: Subclass of "BinaryIO" and "BufferedIOBase" cannot exist: would have incompatible method signatures  [unreachable]
test_open_ut.py: note: In member "test_open_raw_stream" of class "TestOpen":
test_open_ut.py:32: error: Incompatible types in assignment (expression has type "BinaryIO", variable has type "RawIOBase")  [assignment]
test_open_ut.py:33: error: Subclass of "BinaryIO" and "RawIOBase" cannot exist: would have incompatible method signatures  [unreachable]
```

The code for this can be found [here](https://gitlab.com/aucampia/issues/202109-pythonio/-/tree/issue-mypy_open_typing)

Versions:

```
$ poetry show
attrs              21.2.0    Classes Without Boilerplate
black              21.9b0    The uncompromising code formatter.
click              8.0.1     Composable command line interface toolkit
flake8             3.9.2     the modular source code checker: pep8 pyflakes and co
flake8-black       0.2.3     flake8 plugin to call black as a code style validator
flake8-bugbear     21.9.1    A plugin for flake8 finding likely bugs and design problems in your program. Contains warnings that don't belong in pyflakes and pycodestyle.
flake8-isort       4.0.0     flake8 plugin that integrates isort .
importlib-metadata 4.8.1     Read metadata from Python packages
iniconfig          1.1.1     iniconfig: brain-dead simple config-ini parsing
isort              5.9.3     A Python utility / library to sort Python imports.
mccabe             0.6.1     McCabe checker, plugin for flake8
mypy               0.910     Optional static typing for Python
mypy-extensions    0.4.3     Experimental type system extensions for programs checked with the mypy typechecker.
packaging          21.0      Core utilities for Python packages
pathspec           0.9.0     Utility library for gitignore style pattern matching of file paths.
platformdirs       2.3.0     A small Python module for determining appropriate platform-specific dirs, e.g. a "user data dir".
pluggy             1.0.0     plugin and hook calling mechanisms for python
py                 1.10.0    library with cross-python path, ini-parsing, io, code, log facilities
pycodestyle        2.7.0     Python style guide checker
pyflakes           2.3.1     passive checker of Python programs
pyparsing          2.4.7     Python parsing module
pytest             6.2.5     pytest: simple powerful testing with Python
regex              2021.9.24 Alternative regular expression module, to replace re.
testfixtures       6.18.2    A collection of helpers and mock objects for unit tests and doc tests.
toml               0.10.2    Python Library for Tom's Obvious, Minimal Language
tomli              1.2.1     A lil' TOML parser
typed-ast          1.4.3     a fork of Python 2 and 3 ast modules with type comment support
typing-extensions  3.10.0.2  Backported and Experimental Type Hints for Python 3.5+
zipp               3.5.0     Backport of pathlib-compatible object wrapper for zip files
```



