# `open()` overloads mixes `typing.IO` and `io.IOBase` hierarchies.

Currently the overloads for the [`open` builtin](https://github.com/python/typeshed/blob/2217ac8/stdlib/builtins.pyi#L1164-L1249) uses `io.IOBase` based types for specific cases (specifically `io.{TextIOWrapper,FileIO,BufferedRandom,BufferedWriter,BufferedReader}`), but for fallbacks it uses `typing.IO` based types (specifically `typing.{BinaryIO,IO[Any]}`).

The same is the case for [`Path.open`](https://github.com/python/typeshed/blob/2217ac8/stdlib/pathlib.pyi#L89-L143) and [`Traversable.open`](https://github.com/python/typeshed/blob/2217ac8/stdlib/importlib/abc.pyi#L104-L163).

This would maybe not be a problem if the `io.IOBase` classes were considered subclasses, subtypes or implementations of `typing.IO`, but this is not the case. This can maybe be considered a problem on it's own, but it would still seem better to use a consistent approach for typing.

The practical problem that this causes is that functions that should deal with all result from open will have to accept both `typing.IO` and `io.IOBase`.

Written for commit 2217ac8, but can also be seen in typeshed included with mypy 0.910.





  [typeshed/stdlib/io.pyi]: https://github.com/python/typeshed/blob/2217ac8/stdlib/io.pyi
  [typeshed/stdlib/builtins.pyi]: https://github.com/python/typeshed/blob/2217ac8/stdlib/builtins.pyi
  [typeshed/stdlib/builtins.pyi]: https://github.com/python/typeshed/blob/2217ac8/stdlib/builtins.pyi


