# The future of `typing.IO`

I am trying to make the rdflib [ResultSerializer.serialize][rdflib/ResultSerializer.serialize] and [Graph.serialize][rdflib/Graph.serialize] methods more consistent. And in doing so I would like to make them accept and write to objects that are both:
 - BinaryIO "like" streams, specifically objects like those returned from `open(...,"wb", buffering=0)` (which shoudl be ``) and `open(...,"wb")`.
 - TextIO "like" streams, specifically objects like those returned from `open(...,"w")` [[open]].

Some challenges that I face in this:
- Currently mypy does not recognize objects returned by `open()` as subclasses of `io.IOBase`: https://github.com/python/mypy/issues/11193
-





  [rdflib/ResultSerializer.serialize]: https://github.com/RDFLib/rdflib/blob/master/rdflib/query.py#L345
  [rdflib/Graph.serialize]: https://github.com/RDFLib/rdflib/blob/master/rdflib/query.py#L209

  [typeshed/stdlib/io.pyi]: https://github.com/python/mypy/blob/v0.910/mypy/typeshed/stdlib/io.pyi
  [typeshed/stdlib/builtins.pyi]: https://github.com/python/mypy/blob/v0.910/mypy/typeshed/stdlib/builtins.pyi


  [io.RawIOBase]: https://docs.python.org/3.7/library/io.html#io.RawIOBase
  [io.BufferedIOBase]: https://docs.python.org/3.7/library/io.html#io.BufferedIOBase
  [io.TextIOBase]: https://docs.python.org/3.7/library/io.html#io.TextIOBase
  [io.FileIO]: https://docs.python.org/3.7/library/io.html#io.FileIO

  [typing.TextIO]: https://docs.python.org/3.7/library/typing.html#typing.TextIO
  [typing.BinaryIO]: https://docs.python.org/3.7/library/typing.html#typing.BinaryIO

  [open]: https://docs.python.org/3.7/library/functions.html#open

  [202109-pythonio]: https://gitlab.com/aucampia/issues/202109-pythonio/-/tree/master/
  [202109-pythonio/mypy-output.txt]: https://gitlab.com/aucampia/issues/202109-pythonio/-/blob/master/mypy-output.txt
