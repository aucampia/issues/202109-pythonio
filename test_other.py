from io import BufferedIOBase, RawIOBase, TextIOBase
from pathlib import Path
from typing import IO, BinaryIO, TextIO

import pytest


@pytest.fixture(scope="function")
def tmp_file_content() -> str:
    return "some random content"


@pytest.fixture(scope="function")
def tmp_file(tmp_path: Path, tmp_file_content: str) -> Path:
    file_path = tmp_path / "some.txt"
    file_path.write_text("some content")
    return file_path


def test_open_binary_buffered(tmp_file: Path, tmp_file_content: str) -> None:
    with tmp_file.open("rb") as fileh:
        # check types: static
        buffered_iobase: BufferedIOBase = fileh
        # ^ mypy error: Incompatible types in assignment (expression has type
        # "BinaryIO", variable has type "BufferedIOBase")
        binary_io: BinaryIO = fileh
        io_bytes: IO[bytes] = fileh

        # check types: runtime
        assert isinstance(binary_io.read(), bytes)
        assert binary_io is io_bytes
        assert binary_io is buffered_iobase
        # ^ mypy error: on-overlapping identity check (left operand type:
        # "BinaryIO", right operand type: "BufferedIOBase")
        assert not isinstance(fileh, RawIOBase)
        # ^ mypy error: Subclass of "BinaryIO" and "RawIOBase" cannot exist:
        # would have incompatible method signatures  [unreachable]
        assert isinstance(fileh, BufferedIOBase)
        # ^ mypy error: Subclass of "BinaryIO" and "BufferedIOBase" cannot
        # exist: would have incompatible method signatures  [unreachable]


def test_open_binary_unbuffered(tmp_file: Path, tmp_file_content: str) -> None:
    with tmp_file.open("rb", buffering=0) as fileh:
        # check types: static
        buffered_iobase: RawIOBase = fileh
        # ^ mypy error: Incompatible types in assignment (expression has type
        # "BinaryIO", variable has type "RawIOBase")
        binary_io: BinaryIO = fileh
        io_bytes: IO[bytes] = fileh

        # check types: runtime
        assert isinstance(binary_io.read(), bytes)
        assert binary_io is io_bytes
        assert binary_io is buffered_iobase
        # ^ mypy error: Non-overlapping identity check (left operand type:
        # "BinaryIO", right operand type: "RawIOBase")
        assert isinstance(fileh, RawIOBase)
        # ^ mypy error: Subclass of "BinaryIO" and "RawIOBase" cannot exist:
        # would have incompatible method signatures
        assert not isinstance(fileh, BufferedIOBase)
        # ^ mypy error: Statement is unreachable


def test_open_text(tmp_file: Path, tmp_file_content: str) -> None:
    with tmp_file.open("r") as fileh:
        # check types: static
        text_iobase: TextIOBase = fileh
        # ^ mypy error: Incompatible types in assignment (expression has type
        # "TextIO", variable has type "TextIOBase")
        text_io: TextIO = fileh
        io_str: IO[str] = fileh

        # check types: runtime
        assert isinstance(text_io.read(), str)
        assert text_io is io_str
        assert text_io is text_iobase
        # ^ mypy error: Non-overlapping identity check (left operand type:
        # "TextIO", right operand type: "TextIOBase")
        assert isinstance(fileh, TextIOBase)
        # ^ mypy error: Subclass of "TextIO" and "TextIOBase" cannot exist:
        # would have incompatible method signatures
