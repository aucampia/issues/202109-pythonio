Creating python functions that should be able to accept both BinaryIO and TextIO

how to create functions that accepts and writes to both BinaryIO-ish and TextIO-ish objects.





According to typeshed [[typeshed/stdlib/builtins.pyi][typeshed/stdlib/builtins.pyi]:
- `open(...,"wb", buffering=0)` returns [`io.FileIO`][io.FileIO]
- `open(...,"wb")` returns [`io.FileIO`][io.FileIO]




Some obstacles to solving my problem is:
-

- There currently seems to be a parallel type hierarchy for typing.{IO[...], BinaryIO, TextIO} and io.{TextIOBase, BufferedIOBase, RawIOBase}

[[1][typeshed/stdlib/io.pyi]]

```python
class FileIO(RawIOBase, BinaryIO):
   ...
class BytesIO(BufferedIOBase, BinaryIO):
   ...
```


, this should include [io.RawIOBase][io.RawIOBase] and [io.BufferedIOBase][io.BufferedIOBase]
 but should include [io.TextIOBase][io.TextIOBase]



  [typeshed/stdlib/io.pyi]: https://github.com/python/typeshed/blob/2217ac8/stdlib/io.pyi
  [typeshed/stdlib/builtins.pyi]: https://github.com/python/typeshed/blob/2217ac8/stdlib/builtins.pyi
